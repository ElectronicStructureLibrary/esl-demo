| License | Download | Authors |
|:--------|:---------|:--------|
| LGPL-2.1| [ESL-demo](https://gitlab.com/ElectronicStructureLibrary/esl-demo) | ESL team |


The ESL-demo is a self-contained density functional theory (DFT) code with the
purpose of being a "playground" for library maintainers and code developorse.

Its inception was created with the intention of demonstrating 1) how fast a
fully functional DFT code could be developed, 2) that ESL components could
be glued together and 3) a framework for future developments using a common
framework.

At the 5th year of ESL inception the ESL-demonstrator was born and (largely) developed
in a fortnight at a workshop with roughly 10-15 coders.
The ESL-demo is implemented using only ESL components [ESL](ESL "wikilink") and thus
have very little low-level implementations. Almost everything is leveraged by external
libraries.

The DFT implemented is comprising two components both using the pseudopotential methodology
for core states:

1. Plane waves
2. Localized atomic basis orbital

The purpose will not be to replace other DFT codes, but rather testing libraries,
checking interoperability and finding bugs.

Since this uses a large fraction of the libraries shipped in the [ESL bundle](ESL-bundle "wikilink")
it also acts as a test facility for the compatibility of the bundle libraries.




Installation
------------

### Prerequisites

- A C compiler (e.g. gcc, icc, pgcc, xlc, ...)
- A Fortran compiler (e.g. gfortran, ifort, pgif90, xlf, ...)
- A recent Python interpreter (Python 2 >= 2.7.13 or Python 3 >= 3.5.1)
- GNU Make >= 2.80 (other versions may work but have not been tested)


### Instructions

1. Create a `build` directory, and `cd` into it
2. Run `cmake ..`
3. Run `make -j`, this will create the `bin/esl-demo.X` executable

There are some optional flags for the compilation which may be useful
for certain functionalities.

- MPI support: `-DWITH_MPI=ON|OFF`
- ELSI support: `-DWITH_ELSI=ON|OFF`
- Shared libraries: `-DBUILD_SHARED_LIBS=ON|OFF`
- API documentation: `-DWITH_DOC=ON|OFF`


### Tests

In the `tests` directory there are a set of tests that could be runned.
Both atomic orbitals and plane wave tests are added.
Please run at least one of them to see that it runs without problems.


Publications
------------

The ESL-demo and ESL initiative are described here[^1].


References
----------

[^1]: M.J.T Oliveira, N. Papior, Y. Pouillon, *et.al.*,
	*The CECAM electronic structure library and the modular software development paradigm*
    J. Chem. Phys. **153**, 024117 (2020),
    [DOI: 10.1063/5.0012901](https://doi.org/10.1063/5.0012901)
